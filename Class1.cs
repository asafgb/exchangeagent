﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Exchange.Data.Transport;
using Microsoft.Exchange.Data.Transport.Smtp;
namespace MyAgents
{
    public sealed  class MyAgentFactory : SmtpReceiveAgentFactory
    {
        public override SmtpReceiveAgent CreateAgent(SmtpServer server)
        {
            return new MyAgent();
        }
    }
    public class MyAgent : SmtpReceiveAgent
    {
        public MyAgent()
        {
            this.OnEndOfData += new EndOfDataEventHandler(MyEndOfDataHandler);
        }
        private void MyEndOfDataHandler(ReceiveMessageEventSource source, EndOfDataEventArgs e)
        {
            for (int i = 0; i < e.MailItem.Recipients.Count; i++)
            {
                if (e.MailItem.Recipients[i].Address.LocalPart.Contains("+"))
                {
                    e.MailItem.Recipients[i].Address = new RoutingAddress(e.MailItem.Recipients[i].Address.LocalPart.Split('+')[0],
                                                                          e.MailItem.Recipients[i].Address.DomainPart);
                }
            }
        }
    }
}